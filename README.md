# Infinity Engine
This repository will hold the code for how we crawl the Internet and how we make the data that
we retrieve searchable.

## Current Status 
Only a Wikipedia crawler is in this repository but 
we are working to finish the other parts of our system and to add them in here as soon as we can. 

## Using the Wiki Crawler 
All you need to run the [wiki_crawler.py](/wiki_crawler.py) is Python3 and the packages specified 
in the [requirements.txt](/requirements.txt) file.

If you already have [Python3 installed](https://www.python.org/downloads), just run these
commands in your terminal to start the crawler. 
```shell script
git clone https://gitlab.com/infinitysearch/infinity-engine.git
cd infinity-engine
# pip install the required dependencies
pip3 install -r requirements.txt
python3 wiki_crawler.py
```


