import requests
from urllib import parse
from bs4 import BeautifulSoup
from random import choice
import html
import re

desktop_agents = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0']


def random_headers():
    return {'User-Agent': choice(desktop_agents),
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}


def shorten_link(url):
    if url.startswith('//'):
        return url[2:]

    if url.startswith('http://'):
        return url[7:]

    if url.startswith('https://'):
        return url[8:]

    return url


def format_empty_title(url):
    parsed = parse.urlparse(url)
    path = parsed[2]

    if path.endswith('/'):
        path = path[:-1]

    path = path.rsplit('/', 1)[-1]

    title = path.replace('-', ' ')
    title = title.replace('_', ' ')
    title = title.replace('%20', ' ')
    title = html.unescape(title)

    if title.endswith('.html'):
        title = title[:-5]

    if title.endswith('.htm'):
        title = title[:-4]

    return title


def get_title_from_soup(soup):
    try:
        title = soup.title.string
        if title is None:
            title = ''
        title = title.replace('\n', '')
        if title == '':
            new_title = soup.find("meta", property="og:title")['content']
            if new_title is None:
                new_title = ''

            title = new_title.replace('\n', '')

        if title is None:
            title = ''

        return str(title)

    except Exception:
        return ''


def has_js(text):
    if re.search('</script>|</iframe>|</frameset>', text):
        return True
    return False


def get_monetized_from_soup(soup):
    try:
        monetization_pointer = soup.find("meta", attrs={'name': 'monetization'})['content']
    except Exception:
        monetization_pointer = ''

    if monetization_pointer is None:
        monetization_pointer = ''

    return monetization_pointer


def get_description_from_soup(soup):
    desc = ""
    for meta in soup.findAll("meta"):
        try:
            metaname = meta.get('name', '').lower()
            metaprop = meta.get('property', '').lower()
            if 'description' == metaname or metaprop.find("description") > 0:
                desc = meta['content'].strip()
                return desc
        except Exception:
            continue
    return desc


def detect_file_type(url, request):
    url = str(url)

    if url.endswith('.html') or url.endswith('.htm'):
        return 'text'

    if 'content-type' in request.headers:
        return request.headers['content-type']

    if url.endswith('.pdf'):
        return 'application/pdf'

    # Default option
    return 'text'


def get_links_from_soup(soup, url):
    links = []
    try:
        parsed_main_url = parse.urlparse(url)
    except Exception:
        return []
    for link in soup.find_all('a'):
        try:
            link_name = str(link.get('href'))

            if link_name.find('mailto:') >= 0:
                continue

            parsed_link = parse.urlparse(link_name)

            official_link = ''
            if parsed_link[1] != '':
                official_link = link_name
                links.append(official_link)

            elif parsed_link[2] != 'None':
                if parsed_link[2].startswith('/'):
                    official_link = parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_link[2]
                    links.append(official_link)
                else:
                    if parsed_main_url[2].endswith('/'):
                        official_link = parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_main_url[2] + \
                                        parsed_link[2]
                        links.append(official_link)
                    else:
                        official_link = parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_main_url[2] + '/' + \
                                        parsed_link[2]
                        links.append(official_link)

        except Exception:
            continue

    return list(set(links))


def get_page_information(url):
    info = {}
    parsed = parse.urlparse(url)
    homepage = parsed[1]

    try:
        webpage = requests.get(url, timeout=7, headers=random_headers())
    except (requests.exceptions.SSLError, requests.exceptions.ConnectionError, requests.exceptions.RequestException):
        return False, 'Failed To Connect', ''

    status = int(webpage.status_code)
    if status >= 400:
        return False, '>= 400 status code', ''

    filetype = detect_file_type(url, webpage)
    if filetype.find('text') < 0:  # If it is not a text file
        title = format_empty_title(url)

        info['title'] = html.unescape(title)
        info['description'] = ''
        info['ft'] = filetype
        info['has_js'] = ''
        info['status_code'] = webpage.status_code
        info['monetized'] = False
        info['domain'] = homepage
        info['single_domain'] = ".".join(homepage.split('.')[-2:])
        info['website_name'] = homepage.split('.')[-2]
        info['tld'] = '.' + homepage.split('.')[-1]

        return True, info

    webpage_soup = BeautifulSoup(webpage.text, 'html.parser')
    title = get_title_from_soup(webpage_soup)
    description = get_description_from_soup(webpage_soup)

    if len(description) > 200:
        word_array = description[0:200].split()
        if len(word_array) > 2:
            word_array = word_array[0: len(word_array) - 2]
        description = ' '.join(word_array) + ' ...'

    if len(title) > 97:
        title = title[0:97] + ' ...'

    info['title'] = html.unescape(title)
    info['description'] = html.unescape(description)
    info['links'] = get_links_from_soup(webpage_soup, url)
    info['text'] = webpage.text
    info['ft'] = filetype
    info['has_js'] = has_js(webpage.text)
    info['status_code'] = webpage.status_code
    info['monetized'] = get_monetized_from_soup(webpage_soup)
    info['domain'] = homepage
    info['tld'] = '.' + homepage.split('.')[-1]

    return True, info


if __name__ == '__main__':
    webpage_info = get_page_information('https://google.com')
    print(webpage_info)
    import pprint
    pprint.pprint(webpage_info[1])
