import sqlite3
import os
import time
from urllib.parse import urlparse
import requests
from bs4 import BeautifulSoup

'''
* This program crawls Wikipedia and gathers its external links from the bottom of the wiki pages. It also stores the
 Wikipedia urls to track which pages have already been crawled and need to be crawled.

* This crawler can be used to find lots of links about a topic and its related topics quickly by changing the Wikipedia 
 url in the populate_db function to the page that you want to start crawling from. 

* This can also be used to crawl all of Wikipedia, albeit it will take some time to do so. 

* This is built with SQLite so that anyone with Python3 can run it by default and to keep it as simple as possible. This
 database will also never be large enough for there to be any efficiency problems with it. 

* As of 10-26-2020, our formatting and indexing code is not fully completed and is not open source yet. The only 
 information that this crawler stores is the URLS. 
'''

def initiate_db(db_path='websites/websites.db'):
    db = sqlite3.connect(db_path)
    cursor = db.cursor()
    cursor.execute('''CREATE TABLE websites(id INTEGER PRIMARY KEY, url TEXT UNIQUE)''')
    cursor.execute('''CREATE TABLE wikipages(id INTEGER PRIMARY KEY, url TEXT UNIQUE, viewed INTEGER)''')
    cursor.execute('''CREATE INDEX websites_index ON websites(url)''')
    cursor.execute('''CREATE INDEX wikipages_index ON wikipages(url)''')
    db.commit()
    cursor.close()
    db.close()


def populate_db(db_path='websites/websites.db'):
    db = sqlite3.connect(db_path)
    cursor = db.cursor()
    cursor.execute("INSERT INTO wikipages (url, viewed) VALUES ('https://en.wikipedia.org/wiki/Infinity', 0)")
    cursor.execute("INSERT INTO websites (url) VALUES ('infinitysearch.co')")
    db.commit()


def get_links_from_page(url):
    parsed_main_url = urlparse(url)
    links = []
    try:
        page = requests.get(url, timeout=30).text
    except Exception:
        return []
    soup = BeautifulSoup(page, 'html.parser')
    for link in soup.find_all('a'):
        link_name = str(link.get('href'))
        parsed_link = urlparse(link_name)
        if parsed_link[1] != '':
            links.append(link_name)
        elif parsed_link[2] != 'None':
            if parsed_link[2].startswith('/'):
                links.append(parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_link[2])
            else:
                if parsed_main_url[2].endswith('/'):
                    links.append(parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_main_url[2] + parsed_link[2])
                else:
                    links.append(
                        parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_main_url[2] + '/' + parsed_link[2])
    return links


def shorten_link(url):
    if url.startswith('//'):
        return url[2:]
    if url.startswith('http://'):
        return url[7:]
    if url.startswith('https://'):
        return url[8:]
    return url


def wiki_crawler(db_path='websites/websites.db'):
    if os.path.isfile(db_path) is False:
        initiate_db(db_path)
        populate_db(db_path)
    db = sqlite3.connect(db_path)
    cursor = db.cursor()
    # We do not want to add any wiki related links in our list of external websites
    unwanted = ['wikidata.org', 'wiktionary.org', 'wikibooks.org', 'wikiquote.org', 'wikisource.org', 'wikiversity.org',
                'wikimedia.org', 'wikivoyage.org', 'wikinews.org', 'mediawiki.org', 'wikimediafoundation.org',
                'worldcat.org']
    unwanted_strings_wiki = ['en.wikipedia.org/wiki/File:', 'en.wikipedia.org/wiki/Special:',
                             'en.wikipedia.org/wiki/Talk:']
    unwanted_strings_main = ['google.com/search', 'ottobib.com', 'worldcat.org']

    while True:
        cursor.execute("""select * from wikipages where viewed = ?""", (0,))
        count = 0

        all_unviewed_wikipages = cursor.fetchall()
        for wikipage in all_unviewed_wikipages:
            count += 1

            # Just to not send too much to the Wikipedia servers at once
            time.sleep(1)

            shortened_wikipage_url = shorten_link(wikipage[1])
            full_wikipage_url = 'https://' + shortened_wikipage_url

            print('Count:', count)
            print('Wikipage:', wikipage)
            try:
                links = get_links_from_page(full_wikipage_url)
            except Exception:
                print('Error getting links from', full_wikipage_url)
                continue

            for link in links:
                try:
                    if len(link) > 500:
                        continue
                    parsed = urlparse(link)
                    if parsed[0] == '':
                        link = 'https:' + link
                except Exception as e:  # If the link was not in its URL format
                    print(e)
                    continue

                shortened_link = shorten_link(link)
                # If it is a Wikipedia link and wiki page
                if parsed[1].find('en.wikipedia.org') >= 0 and parsed[2].find('/wiki') >= 0:
                    passed = True
                    for full_path in unwanted_strings_wiki:
                        if shortened_link.find(full_path) >= 0:
                            passed = False
                            break
                    if passed is False:
                        continue

                    try:
                        cursor.execute("""select * from wikipages where url = ?""", (shortened_link,))
                        wiki_links = list(cursor.fetchall())
                    except Exception as e:
                        print(e)
                        wiki_links = []

                    # If we already have this link in our index
                    if len(wiki_links) > 0:
                        continue

                    print('Unseen Wiki Page:', shortened_link)
                    cursor.execute("""INSERT INTO wikipages (url, viewed) VALUES (?, ?)""", (shortened_link, 0))

                    db.commit()
                    continue

                # For non wikipedia links
                if str(parsed[1]).find('wikipedia.org') >= 0:
                    continue

                passed = True
                for domain in unwanted:  # Excluding wiki related sites
                    if parsed[1].find(domain) >= 0:
                        passed = False
                        break

                if passed is False:
                    continue

                for full_path in unwanted_strings_main:
                    if shortened_link.find(full_path) >= 0:
                        passed = False
                        break

                if passed is False:
                    continue

                cursor.execute("""select * from websites where url = ?""", (shortened_link,))
                existing_links = list(cursor.fetchall())
                if len(existing_links) > 0:  # If we already have the link
                    continue

                print('Passed Link:', link)
                cursor.execute("""INSERT INTO websites (url) VALUES (?)""", (shortened_link,))
                db.commit()

            cursor.execute("""UPDATE wikipages set viewed = 1 where id = ?""", (wikipage[0],))
            db.commit()

        if count == 0:
            print('All of Wikipedia has been crawled! Exiting program')
            cursor.close()
            db.close()
            exit(0)


# Quick function to view the database contents
def view_database_contents(db_path='websites/websites.db'):
    db = sqlite3.connect(db_path)
    cursor = db.cursor()
    cursor.execute("SELECT * FROM wikipages")
    wikipages = list(cursor.fetchall())
    cursor.execute("SELECT * FROM websites")
    websites = list(cursor.fetchall())

    print('Number of Wikipedia pages in the database:', len(wikipages))
    print('Number of external links in the database:', len(websites))

    print('External URLs:')
    for website in websites:
        print(website)

    print('Wikipages:')
    for wikipage in wikipages:
        print(wikipage)


if __name__ == '__main__':
    # view_database_contents('websites/websites_example.db')
    wiki_crawler('websites/websites_example.db')
